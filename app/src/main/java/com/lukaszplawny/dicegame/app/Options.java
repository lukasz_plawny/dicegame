package com.lukaszplawny.dicegame.app;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class Options extends ActionBarActivity {
    private static int balance = 20;
    private EditText balanceInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        balanceInput = (EditText) findViewById(R.id.TbalanceInput);
        balanceInput.requestFocus();
    }

    public void set(View v){
        try{
            balance = Integer.parseInt(balanceInput.getText().toString());
            finish();
        }
        catch(Exception e)
        {
            balanceInput.setHint("Wpisz liczbę");
        }
    }

    public static int getRate() {
        return balance;
    }

    public void escape(View v) {
        finish();
    }
}