package com.lukaszplawny.dicegame.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends ActionBarActivity {
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
    }
    public void escape(View v) {
        finish();
    }

    public void start(View v) {
        Intent intent = new Intent(context, Game.class);
        startActivity(intent);
    }

    public void showOptions(View v) {
        Intent intent = new Intent(context, Options.class);
        startActivity(intent);
    }
}
