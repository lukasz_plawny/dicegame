package com.lukaszplawny.dicegame.app;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Random;

public class Game extends ActionBarActivity {
    private int bid = 0, // kwota do stracenia
            win = 0, // kwota do wygrania
            radioCheckedId, balance;
    private static Integer lastDice1 = 0, lastDice2 = 0, // wynik wcześniejszego
    // rzutu
    currentDice1 = 0, currentDice2 = 0; // wynik obecnego rzutu
    private EditText bidText;
    private TextView awardText, balanceText, alert;
    private RadioGroup radioGroup;
    private RadioButton less, more, equal;
    private Button throwButton;
    private ImageView dice1, dice2;
    private AnimationDrawable animDice1, animDice2;
    private Random random = new Random();
    private android.os.Handler handler = new android.os.Handler();
    private boolean firstShot;

    @Override
    protected void onCreate(Bundle savedInbalanceTextceState) {
        super.onCreate(savedInbalanceTextceState);
        setContentView(R.layout.activity_game);
        bidText = (EditText) findViewById(R.id.TbidInput);
        throwButton = (Button) findViewById(R.id.Bthrow);
        radioGroup = (RadioGroup) findViewById(R.id.RGbet);
        less = (RadioButton) findViewById(R.id.Bless);
        more = (RadioButton) findViewById(R.id.Bmore);
        equal = (RadioButton) findViewById(R.id.Bequal);
        awardText = (TextView) findViewById(R.id.TwinOutput);
        alert = (TextView) findViewById(R.id.Talert);
        balanceText = (TextView) findViewById(R.id.TbalanceOutput);
        firstShot = true;
        balance = Options.getRate();
        balanceText.setText(Integer.toString(balance));
        dice1 = (ImageView) findViewById(R.id.dice1);
        dice2 = (ImageView) findViewById(R.id.dice2);
        bidText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                calculateWin();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                calculateWin();

            }
        });
    }

    public void escape(View v) {
        finish();
    }

    public void throwDice(View v) { // uruchamia animacje, rzuca koścmi
        if (firstShot) {
            dice1.setBackgroundResource(R.anim.dice_animation);
            dice2.setBackgroundResource(R.anim.dice_animation);
            animDice1 = (AnimationDrawable) dice1.getBackground();
            animDice1.start();
            animDice2 = (AnimationDrawable) dice2.getBackground();
            animDice2.start();
            lastDice1 = Integer.valueOf(new Random().nextInt(6) + 1);
            lastDice2 = Integer.valueOf(new Random().nextInt(6) + 1);
            while (lastDice1 == lastDice2 && (lastDice1 == 1 || lastDice2 == 6)) {
                lastDice1 = Integer.valueOf(new Random().nextInt(6) + 1);
                lastDice2 = Integer.valueOf(new Random().nextInt(6) + 1);
            }

            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    showResultOnDice(lastDice1, lastDice2, true);
                }
            }, 2000);

            firstShot = false;
            betEnable(true);
            throwButton.setText("Drugi rzut");
        } else {
            try {
                Integer.parseInt(bidText.getText().toString());
                alert.setText("");
                dice1.setBackgroundResource(R.anim.dice_animation);
                dice2.setBackgroundResource(R.anim.dice_animation);
                animDice1 = (AnimationDrawable) dice1.getBackground();
                animDice1.start();
                animDice2 = (AnimationDrawable) dice2.getBackground();
                animDice2.start();
                currentDice1 = random.nextInt(6) + 1;
                currentDice2 = random.nextInt(6) + 1;
                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        showResultOnDice(currentDice1, currentDice2, true);
                        afterThrowCalculate(lastDice1 + lastDice2, currentDice1
                                + currentDice2);
                    }
                }, 3000);

                firstShot = true;
                betEnable(false);
                throwButton.setText("Pierwszy rzut");

            } catch (Exception e) {
                alert.setText("Wpisz stawianą kwotę");
            }
        }
    }

    private void showResultOnDice(int firstDice, int secondDice,
                                  boolean stopAnimation) { // pokazuje przekazaną liczbę oczek na
        // kościach
        if (stopAnimation) {
            animDice1.stop();
            animDice2.stop();
        }
        switch (firstDice) {
            case 1:
                dice1.setBackgroundResource(R.drawable.kl1);
                break;
            case 2:
                dice1.setBackgroundResource(R.drawable.kl2);
                break;
            case 3:
                dice1.setBackgroundResource(R.drawable.kl3);
                break;
            case 4:
                dice1.setBackgroundResource(R.drawable.kl4);
                break;
            case 5:
                dice1.setBackgroundResource(R.drawable.kl5);
                break;
            case 6:
                dice1.setBackgroundResource(R.drawable.kl6);
                break;
        }
        switch (secondDice) {
            case 1:
                dice2.setBackgroundResource(R.drawable.kl2);
                break;
            case 2:
                dice2.setBackgroundResource(R.drawable.kl2);
                break;
            case 3:
                dice2.setBackgroundResource(R.drawable.kl3);
                break;
            case 4:
                dice2.setBackgroundResource(R.drawable.kl4);
                break;
            case 5:
                dice2.setBackgroundResource(R.drawable.kl5);
                break;
            case 6:
                dice2.setBackgroundResource(R.drawable.kl6);
                break;
        }
    }

    private void afterThrowCalculate(int a, int b) { // sprawdza czy gracz
        // wygrał, dodaje
        // wygraną/odejmuje
        // przegraną
        if ((a > b && radioGroup.getCheckedRadioButtonId() == less.getId()) // gracz
                || (a < b && radioGroup.getCheckedRadioButtonId() == more.getId()) // wygrywa
                || (a == b && radioGroup.getCheckedRadioButtonId() == equal.getId())) {

            balance += win;
            balanceText.setText(Integer.toString(balance));
        } else { // gracz przegrywa
            balance -= bid;
            if (balance == 0) {
                alert.setText("Koniec Gry");
                betEnable(false);
                throwButton.setClickable(false);
            }
            balanceText.setText(Integer.toString(balance));

        }
        awardText.setText("0");
        bidText.setText("");
        bid = 0;
        win = 0;
    }

    private void betEnable(boolean setEnabled) {
        bidText.setEnabled(setEnabled);
        radioGroup.setClickable(setEnabled);
        less.setEnabled(setEnabled);
        more.setEnabled(setEnabled);
        equal.setEnabled(setEnabled);
    }

    private int getDices() {
        return (lastDice1 + lastDice2);
    }

    private void calculateWin() {
        if (!bidText.getText().toString().isEmpty()) {
            bid = Integer.parseInt(bidText.getText().toString());
            if (bid > balance) {
                bid = balance;
                bidText.setText(Integer.toString(bid));
                calculateWin();
                return;
            } else {
                radioCheckedId = radioGroup.getCheckedRadioButtonId();
                if (radioCheckedId == equal.getId()) {
                    if (getDices() < 5 || getDices() > 9)
                        win = bid * 10;
                    else
                        win = bid * 5;
                } else {
                    if (getDices() >= 10) {
                        if (radioCheckedId == more.getId())
                            win = bid * 10;
                        else
                            win = bid;
                    }
                    if (getDices() <= 4) {
                        if (radioCheckedId == more.getId()) {
                            win = bid;
                        } else
                            win = bid * 10;
                    }
                    if ((getDices() == 5) || (getDices() == 6)) {
                        if (radioCheckedId == more.getId())
                            win = bid * 2;
                        else
                            win = bid * 3;
                    }
                    if ((getDices() == 8) || (getDices() == 9)) {
                        if (radioCheckedId == more.getId())
                            win = bid * 3;
                        else
                            win = bid * 2;
                    }
                    if (getDices() == 7)
                        win = bid * 2;
                }
            }
        } else {
            bid = 0;
            win = 0;
        }
        awardText.setText(Integer.toString(win));
    }
}
